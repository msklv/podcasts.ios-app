//
//  PodcastSearchController.swift
//  Podcasts
//
//  Created by Павел Москалёв on 20.02.22.
//

import Foundation
import UIKit

class PodcastSearchController : UITableViewController, UISearchBarDelegate {
    
    var findResult : [Podcast] = []
    let cellId = "cellId"
    let searchController = UISearchController(searchResultsController: nil)
    var textSearch = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupTableView()
        searchBar(searchController.searchBar, textDidChange: "Подкасты")
    }
    
    // MARK: - Setup Methods
    
    fileprivate func setupSearchBar() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
    }
    
    fileprivate func setupTableView() {
        tableView.tableFooterView = UIView()
        let nib = UINib(nibName: "PodcastCell" , bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
    }
    
    // MARK: - UISearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        findResult = []
        tableView.reloadData()
        textSearch = searchText
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(loadSearchResult), object: nil)
        self.perform(#selector(loadSearchResult), with: nil, afterDelay: 0.5)
    }
    
    @objc func loadSearchResult() {
        NetworkManager.shared.fetchPodcast(search : textSearch ) { result, sucsess, error in
            if sucsess {
                if let result = result?.results {
                    self.findResult = result.filter( {$0.feedUrl != nil})
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - UITableView
    
    var podcastSearchView = Bundle.main.loadNibNamed("PodcastsSearchingView", owner: PodcastSearchController.self, options: nil)?.first as? UIView
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return podcastSearchView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return findResult.isEmpty ? 200 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return findResult.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PodcastCell
        let podcast = self.findResult[indexPath.row]
        cell.podcast = podcast
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let episodesVC = EpisodesController()
        let podcast = self.findResult[indexPath.row]
        episodesVC.podcast = podcast
        navigationController?.pushViewController(episodesVC, animated: true)
    }
}
