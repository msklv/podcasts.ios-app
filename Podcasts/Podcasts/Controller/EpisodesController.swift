//
//  EpisodesController.swift
//  Podcasts
//
//  Created by Павел Москалев on 4.03.22.
//

import UIKit

class EpisodesController: UITableViewController {
    
    var podcast : Podcast?
    let cellId = "cellId"
    var episodes = [Episode]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        fetchEpisode()
        setupNavBarButton()
    }
   
    func setupNavBarButton() {
        let savedPodcasts = UserDefaults.standard.savedPodcast()
        let hasFavorited = savedPodcasts.firstIndex(where: {$0.trackName == self.podcast?.trackName  && $0.artistName == self.podcast?.artistName}) != nil
            if hasFavorited {
                navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "heart"), style: .plain, target: nil, action: nil)
            } else {
                navigationItem.rightBarButtonItems = [UIBarButtonItem(title: "Favorite", style: .plain, target: self, action: #selector(handleSaveFavorite))]
            }
    }
    
    @objc func handleSaveFavorite() {
        guard let podcast = self.podcast else { return }
        var listOfPodcasts = UserDefaults.standard.savedPodcast()
        listOfPodcasts.append(podcast)
        let data = NSKeyedArchiver.archivedData(withRootObject: listOfPodcasts)
        UserDefaults.standard.set(data, forKey: UserDefaults.favoritePodcastKey)
        showBadgeHighlight()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "heart"), style: .plain, target: nil, action: nil)
    }
    
    func showBadgeHighlight() {
        UIApplication.mainTabBarController()?.viewControllers?[0].tabBarItem.badgeValue = "New"
    }
    
    @objc func handleFetchSavedFavoritePodcast() {

        guard let data = UserDefaults.standard.data(forKey: UserDefaults.favoritePodcastKey) else { return }
        let savedPodcast = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Podcast]
        
        savedPodcast?.forEach({ (p) in
            print(p.trackName)
        })
    }
    
    
    func setupUI() {
        let nib = UINib(nibName: "EpisodeCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
        navigationItem.title = podcast?.trackName
    }
    
    
    func fetchEpisode() {
        print("Looking for episodes at feed url:", podcast?.feedUrl ?? "")
        guard let feedUrl = podcast?.feedUrl else { return }
        NetworkManager.shared.fetchEpisodes(feedUrl: feedUrl) { (episodes) in
            self.episodes = episodes
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let downloadAction = UITableViewRowAction(style: .normal, title: "Download") {_,_ in
            let episode = self.episodes[indexPath.row]
            UserDefaults.standard.downloadEpisode(episode: episode)
                    print("Download Finish")
            NetworkManager.shared.downloadEpisode(episode: episode)
            
        }
        return [downloadAction]
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let activity = UIActivityIndicatorView()
        activity.style = .large
        activity.color = .darkGray
        activity.startAnimating()
        return activity
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return episodes.isEmpty ? 200 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return episodes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EpisodeCell
        let episode = self.episodes[indexPath.row]
        cell.prepareCell(episode:episode)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let episode = self.episodes[indexPath.row]
        let mainTabBarController = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController as? MainTabBarController
        mainTabBarController?.maximizePlayerDetails(episode: episode, playerListEpisode: self.episodes)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
}
