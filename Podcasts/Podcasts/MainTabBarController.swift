//
//  MainTabBarController.swift
//  Podcasts
//
//  Created by Павел Москалёв on 19.02.22.
//

import UIKit

class MainTabBarController : UITabBarController {
    
    var maxTopAnchorConstraint: NSLayoutConstraint!
    var minTopAnchorConstraint: NSLayoutConstraint!
    let playerDetailsView = PlayerDetailsView.initFromNib()
    var bottoAnchorConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupPlayersDetailsView()
    }
    
    @objc func minimizePlayerDetails() {
        maxTopAnchorConstraint.isActive = false
        bottoAnchorConstraint.constant = view.frame.height
        minTopAnchorConstraint.isActive = true
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut) {
            self.view.layoutIfNeeded()
            self.tabBar.isHidden = false
            self.playerDetailsView.fullScreenStackView.alpha = 0
            self.playerDetailsView.miniScreenView.alpha = 1
        }
    }
    
    func maximizePlayerDetails(episode: Episode?, playerListEpisode : [Episode] = []) {
        minTopAnchorConstraint.isActive = false
        maxTopAnchorConstraint.isActive = true
        maxTopAnchorConstraint.constant = 0
        bottoAnchorConstraint.constant = 0
        if episode != nil {
            playerDetailsView.episode =  episode
        }
        
        playerDetailsView.playListEpisode = playerListEpisode
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut) {
            self.view.layoutIfNeeded()
            self.tabBar.isHidden = true
            self.playerDetailsView.fullScreenStackView.alpha = 1
            self.playerDetailsView.miniScreenView.alpha = 0
        }
    }
    
    // MARK: - Setup Methods
    
    func setupPlayersDetailsView() {
        view.insertSubview(playerDetailsView, belowSubview: tabBar)
        playerDetailsView.translatesAutoresizingMaskIntoConstraints = false
        maxTopAnchorConstraint =  playerDetailsView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height)
        maxTopAnchorConstraint.isActive = true
        minTopAnchorConstraint = playerDetailsView.topAnchor.constraint(equalTo: tabBar.topAnchor, constant: -64)
        playerDetailsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottoAnchorConstraint = playerDetailsView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.frame.height)
        bottoAnchorConstraint.isActive = true
        playerDetailsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    func setupNavBar() {
        UINavigationBar.appearance().prefersLargeTitles = true
        tabBar.tintColor = .purple
        let layout = UICollectionViewFlowLayout()
        let favoritesController = FavoritesController(collectionViewLayout: layout)
        viewControllers = [   generationNavController(rootViewController: favoritesController, title: "Favorites", image: UIImage(named: "favorites")!),
            generationNavController(rootViewController: PodcastSearchController(), title: "Search", image: UIImage(named: "search")!), generationNavController(rootViewController: DownloadsController(), title: "Downloads", image: UIImage(named: "downloads")!)]
    }
    
    // MARK: -  Helpers Methods
    
    fileprivate func generationNavController( rootViewController : UIViewController , title :String , image : UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        rootViewController.navigationItem.title = title
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        return navController
    }
}
