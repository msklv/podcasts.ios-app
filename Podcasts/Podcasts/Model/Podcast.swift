//
//  Podcast.swift
//  Podcasts
//
//  Created by Павел Москалёв on 20.02.22.
//

import Foundation

// MARK: - SearchResult

struct PodcastsList : Codable {
    var resultCount: Int?
    var results: [Podcast]?
}

class Podcast  : NSObject, Codable , NSCoding {
    
    func encode(with coder: NSCoder) {
        print("trying to transform Podcast into Data")
        coder.encode(trackName ?? "", forKey: "trackNameKey")
        coder.encode(artistName ?? "", forKey: "artistNameKey")
        coder.encode(artworkUrl600 ?? "" , forKey: "artworkKey")
        coder.encode(feedUrl ?? "" , forKey: "feedUrl")

    }
    
    required init?(coder: NSCoder) {
        print("trying  Podcast into Data")
        self.trackName = coder.decodeObject(forKey: "trackNameKey") as? String
        self.artistName = coder.decodeObject(forKey: "artistNameKey") as? String
        self.artworkUrl600 = coder.decodeObject(forKey: "artworkKey") as? String
        self.feedUrl = coder.decodeObject(forKey: "feedUrl") as? String
    }
    
    var trackName : String?
    var artistName : String?
    var artworkUrl30 : String?
    var artworkUrl60 : String?
    var artworkUrl100 : String?
    var artworkUrl600 : String?
    var trackCount : Int?
    var feedUrl : String?
}
