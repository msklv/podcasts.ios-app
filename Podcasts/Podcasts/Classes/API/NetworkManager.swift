//
//  NetworkManager.swift
//  Podcasts
//
//  Created by Павел Москалёв on 20.02.22.
//

import Foundation
import Alamofire
import FeedKit

//MARK: - Search

class NetworkManager {
    
    //singleton
    static let shared = NetworkManager()
    
    func fetchPodcast(search: String, completion: @escaping (PodcastsList?, Bool, String?) -> Void) {
        let parameters = ["term" :search, "media": "podcast"]
        let url = "https://itunes.apple.com/search"
        let request = AF.request(url, method: .get, parameters: parameters as Parameters , encoding: URLEncoding.default)
        request.responseDecodable(of: PodcastsList.self) { (response) in
            switch response.result {
            case .failure(let error):
                completion(nil,false,String(describing: error))
            case .success(let data):
                completion(data,true,nil)
            }
        }
    }
    
    func fetchEpisodes(feedUrl: String, completionHandler: @escaping ([Episode]) -> ()) {
        let secureFeedUrl = feedUrl.contains("https") ? feedUrl : feedUrl.replacingOccurrences(of: "http", with: "https")
        guard let url = URL(string: secureFeedUrl) else { return }
        DispatchQueue.global(qos: .background).async {
            let parser = FeedParser(URL: url)
            let result = parser.parse()
            switch result {
            case .success(let feed):
                guard let feed = feed.rssFeed else {return}
                let episodes = feed.toEpisodes()
                completionHandler(episodes)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func downloadEpisode(episode: Episode) {
        let url = episode.streamUrl
        let downloadRequest = DownloadRequest.suggestedDownloadDestination()

        AF.download(url, to: downloadRequest).response { responseData in
                switch responseData.result {
                case .success(let url):
                    guard let url = url else { return }
//                    self.urlLoad.append(url)
                    var downloadedEpisodes = UserDefaults.standard.downloadedEpisodes()
                    guard let index = downloadedEpisodes.firstIndex(where: { $0.title == episode.title && $0.author == episode.author }) else { return }
                    downloadedEpisodes[index].fileUrl = url.absoluteString
                    do {
                        
                        let data = try JSONEncoder().encode(downloadedEpisodes)
                        UserDefaults.standard.set(data, forKey: UserDefaults.downloadEpisodeKey)
                    } catch let err {
                        print("Failed to encode downloaded episodes with file url update:", err)
                    }
                case .failure(let error):
                    print(error)
                }
            }
    }
}
