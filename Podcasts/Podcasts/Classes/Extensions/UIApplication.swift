//
//  UIApplication.swift
//  Podcasts
//
//  Created by Павел Москалев on 16.03.22.
//

import Foundation
import UIKit

extension UIApplication {
    static func mainTabBarController() -> MainTabBarController? {
        return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController as? MainTabBarController
    }
}
