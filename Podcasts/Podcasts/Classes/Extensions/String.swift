//
//  String.swift
//  Podcasts
//
//  Created by Павел Москалев on 5.03.22.
//

import Foundation

extension String {
    func toSecureHTTPS() -> String {
        return self.contains("https") ? self : self.replacingOccurrences(of: "http", with: "https")
    }
}
