//
//  PodcastCell.swift
//  Podcasts
//
//  Created by Павел Москалев on 2.03.22.
//

import UIKit
import Kingfisher

class PodcastCell: UITableViewCell {

    @IBOutlet weak var podcastImage: UIImageView!
    @IBOutlet weak var podcastTrackArtist: UILabel!
    @IBOutlet weak var podcastsNameArtist: UILabel!
    @IBOutlet weak var podcastCountEpisode: UILabel!
    
    var podcast : Podcast! {
        didSet {
            podcastTrackArtist.text = podcast.trackName
            podcastsNameArtist.text = podcast.artistName
            podcastCountEpisode.text = "\(podcast.trackCount ?? 0 ) Episodes"
            guard let url = URL(string: podcast.artworkUrl100 ?? "") else {
                podcastImage.image = UIImage(named: "podcastImage")
                return}
            podcastImage.kf.indicatorType = .activity
            podcastImage.kf.setImage(with: url)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
