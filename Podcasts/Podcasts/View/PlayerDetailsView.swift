//
//  PlayerDetailsView.swift
//  Podcasts
//
//  Created by Павел Москалев on 6.03.22.
//

import Foundation
import UIKit
import Kingfisher
import AVKit
import MediaPlayer

class PlayerDetailsView: UIView {
    
    let scale = CGAffineTransform(scaleX: 0.7, y: 0.7)
    var panGesture: UIPanGestureRecognizer!
    var playListEpisode = [Episode]()
    
    // mini screen player
    @IBOutlet weak var miniScreenView: UIView!
    @IBOutlet weak var miniIconImageView: UIImageView!
    @IBOutlet weak var miniTitleLabel: UILabel!
    @IBOutlet weak var miniFastForwardButton: UIButton!
    @IBOutlet weak var miniPlayPauseButton: UIButton! {
        didSet {
            miniPlayPauseButton.addTarget(self, action: #selector(playPauseButtonAction), for: .touchUpInside)
        }
    }
    
    // full screen player
    
    @IBOutlet weak var fullScreenStackView: UIStackView!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var currentTimeSlider: UISlider!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    {
        didSet {
            playPauseButton.addTarget(self, action: #selector(playPauseButtonAction), for: .touchUpInside)
        }
    }
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var episodeImageView: UIImageView! {
        didSet {
            smallEpisodeImageView()
        }
    }
    @IBOutlet weak var episodeTitleLabel: UILabel!
    
    let player : AVPlayer = {
        let avPlayer = AVPlayer()
        avPlayer.automaticallyWaitsToMinimizeStalling = false
        return avPlayer
    }()
    
    var episode: Episode? {
        didSet {
            setupUI()
        }
    }
    
    fileprivate func observerPlayerCurrentTiem() {
        let interval = CMTimeMake(value: 1, timescale: 2)
        player.addPeriodicTimeObserver(forInterval: interval, queue: .main) {  [weak self] time  in
            self?.currentTimeLabel.text = time.toDisplayString()
            let durationTime = self?.player.currentItem?.duration
            self?.durationLabel.text = durationTime?.toDisplayString()
            self?.updateCurrentTimeSlider()
        }
    }

    func updateCurrentTimeSlider() {
        let currentTimeSec = CMTimeGetSeconds(player.currentTime())
        let durationSec = CMTimeGetSeconds(player.currentItem?.duration ?? CMTimeMake(value: 1, timescale: 1))
        let percent = currentTimeSec / durationSec
        self.currentTimeSlider.value = Float(percent)
    }
    
    func setupAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let sessionErr {
            print("Failed", sessionErr)
        }
    }
    
    func setupRemoteControl() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.playCommand.isEnabled = true
        commandCenter.playCommand.addTarget { [self] (_)
            -> MPRemoteCommandHandlerStatus in
            print("play podcast...")
            self.player.play()
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            miniPlayPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            self.setupElapsedTime(playbackRate: 1)
            return .success
        }
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.pauseCommand.addTarget { [self] (_)
            -> MPRemoteCommandHandlerStatus in
            print("pause podcast...")
            self.player.pause()
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            miniPlayPauseButton.setImage(UIImage(named: "play"), for: .normal)
            self.setupElapsedTime(playbackRate: 0)
            return .success
        }
        commandCenter.togglePlayPauseCommand.isEnabled = true
        commandCenter.togglePlayPauseCommand.addTarget { (_)
            -> MPRemoteCommandHandlerStatus in
            self.playPauseButtonAction()
            return .success
        }
        commandCenter.previousTrackCommand.addTarget(self , action: #selector(handlePreviousTrack))

        commandCenter.nextTrackCommand.addTarget(self , action: #selector(handleNextTrack))
    }
    
    @objc func handlePreviousTrack() ->  MPRemoteCommandHandlerStatus {
        if playListEpisode.count == 0 {
            return .commandFailed
        }
        let currentEpisodeIndex = playListEpisode.firstIndex { (ep) -> Bool in
            return self.episode!.title == ep.title && self.episode!.author == ep.author
        }
        
        guard let index = currentEpisodeIndex else { return .commandFailed}
        let nextEpisode : Episode
        if index == 0 {
            nextEpisode = playListEpisode[playListEpisode.count - 1]
        } else {
            nextEpisode = playListEpisode[index - 1]
        }
        self.episode = nextEpisode
        return .success
    }
    
    @objc func handleNextTrack() ->  MPRemoteCommandHandlerStatus {
        if playListEpisode.count == 0 {
            return .commandFailed
        }
        let currentEpisodeIndex = playListEpisode.firstIndex { (ep) -> Bool in
            return self.episode!.title == ep.title && self.episode!.author == ep.author
        }
        
        guard let index = currentEpisodeIndex else { return .commandFailed}
        let nextEpisode : Episode
        if index == playListEpisode.count - 1 {
            nextEpisode = playListEpisode[0]
        } else {
            nextEpisode = playListEpisode[index + 1]

        }
        self.episode = nextEpisode
        return .success
    }
    
    func setupElapsedTime(playbackRate : Float) {
        let elapsedTime =  CMTimeGetSeconds(player.currentTime())
        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = elapsedTime
        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = playbackRate

    }
    
    fileprivate func observeBoundaryTime() {
        let time = CMTimeMake(value: 1, timescale: 3)
        let times = [NSValue(time: time)]
        player.addBoundaryTimeObserver(forTimes: times, queue: .main) { [weak self] in
            self?.largeEpisodeImageView()
            self?.playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            self?.setupLockScreenDuration()
        }
    }
    
    func setupLockScreenDuration() {
        guard let duration = player.currentItem?.duration else { return }
        let durationSeconds = CMTimeGetSeconds(duration)
        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = durationSeconds
    }
    
    func setupInterruptionObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleInterruption), name: AVAudioSession.interruptionNotification, object: nil)
    }
    
    @objc func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let type = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt else { return }
        if type == AVAudioSession.InterruptionType.began.rawValue {
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            miniPlayPauseButton.setImage(UIImage(named: "play"), for: .normal)
        } else {
            guard let options = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }
            if options == AVAudioSession.InterruptionOptions.shouldResume.rawValue {
                player.play()
                playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
                miniPlayPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupRemoteControl()
        setupGesture()
        setupInterruptionObserver()
        observerPlayerCurrentTiem()
        
        observeBoundaryTime()
    }
    
    func setupBackgroundScreenPlayer() {
        var nowPlayingInfo = [String : Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = episode?.title
        nowPlayingInfo[MPMediaItemPropertyArtist] = episode?.author
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func setupGesture() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapMaximize)))
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        miniScreenView.addGestureRecognizer(panGesture)
        fullScreenStackView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismissFullScreen)))
    }
    
    @objc func handleDismissFullScreen(gesture: UIPanGestureRecognizer) {
        if gesture.state == .changed {
            let translation = gesture.translation(in: superview)
            fullScreenStackView.transform = CGAffineTransform(translationX: 0, y: translation.y)
        } else if gesture.state == .ended {
            let translation = gesture.translation(in: superview)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut) {
                self.fullScreenStackView.transform = .identity
                if translation.y > 200 {
                    UIApplication.mainTabBarController()?.minimizePlayerDetails()
                }
            }
        }
    }
    
    static func initFromNib() -> PlayerDetailsView {
        return Bundle.main.loadNibNamed("PlayerDetailsView", owner: self, options: nil)?.first as! PlayerDetailsView
    }
    
    func setupUI() {
        authorLabel.text = episode?.author
        episodeTitleLabel.text = episode?.title
        let url = URL(string: episode?.imageUrl?.toSecureHTTPS() ?? "")
        episodeImageView.kf.indicatorType = .activity
        episodeImageView.kf.setImage(with: url)
        episodeImageView.layer.cornerRadius = 10
        episodeImageView.clipsToBounds = true
        setupBackgroundScreenPlayer()
        setupMiniScreenUI()
        setupAudioSession()
        playEpisode()
    }
    
    func setupMiniScreenUI() {
        miniTitleLabel.text = episode?.title
        guard let url = URL(string: episode?.imageUrl?.toSecureHTTPS() ?? "") else { return }
        miniIconImageView.kf.indicatorType = .activity
        miniIconImageView.kf.setImage(with: url) { _ in
            let image : UIImage = self.miniIconImageView.image!
            var nowPlayingInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
            let artwork = MPMediaItemArtwork(boundsSize: image.size) { (_) in
                return image
            }
            nowPlayingInfo?[MPMediaItemPropertyArtwork] = artwork
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        }
    }
    
    func playEpisode() {
        
        if episode?.fileUrl != nil {
            playEpisodeUsingFileUrl()
        } else {
            guard let url = URL(string: episode?.streamUrl ?? "") else {return}
            let playerItem = AVPlayerItem(url: url)
            player.replaceCurrentItem(with: playerItem)
            player.play()
        }
    }
    
    func playEpisodeUsingFileUrl() {
        guard let fileURL = URL(string: episode?.fileUrl ?? "") else { return }
        let fileName = fileURL.lastPathComponent
        guard var trueLocation = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        print(trueLocation.absoluteString)
        trueLocation.appendPathComponent(fileName)
        let playerItem = AVPlayerItem(url: trueLocation)
        player.replaceCurrentItem(with: playerItem)
        player.play()
    }
    
    func largeEpisodeImageView() {
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut) {
            self.episodeImageView.transform = .identity
        }
    }
    
    func smallEpisodeImageView() {
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut) { [self] in
            episodeImageView.transform = scale
        }
    }
    
    @IBAction func handleCurrentTimeSliderChanged(_ sender: UISlider) {
        let percent = currentTimeSlider.value
        guard let duration =  player.currentItem?.duration else {return}
        let durationSec = CMTimeGetSeconds(duration)
        let seekTimeInSecond = Float64(percent) * durationSec
        let seekTime = CMTimeMakeWithSeconds(seekTimeInSecond, preferredTimescale: 1)
        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = seekTimeInSecond
        player.seek(to: seekTime)
    }
    
    @IBAction func handleRewind(_ sender: UIButton) {
        seekToCurrentTime(-15)
    }
    
    @IBAction func handleFastForward(_ sender: UIButton) {
        seekToCurrentTime(15)
    }
    
    func seekToCurrentTime(_ delta : Int64) {
        let fifteen = CMTimeMake(value: delta, timescale: 1)
        let seekTime = CMTimeAdd(player.currentTime(), fifteen)
        player.seek(to: seekTime)
    }
    
    @IBAction func volumeChangeSlider(_ sender: UISlider) {
        player.volume = volumeSlider.value
    }
    
    @objc func playPauseButtonAction() {
        if player.timeControlStatus == .paused {
            player.play()
            largeEpisodeImageView()
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            miniPlayPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            self.setupElapsedTime(playbackRate: 1)
        } else {
            player.pause()
            smallEpisodeImageView()
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            miniPlayPauseButton.setImage(UIImage(named: "play"), for: .normal)
            self.setupElapsedTime(playbackRate: 0)
        }
    }
    
    @IBAction func dismissButton(_ sender: UIButton) {
        UIApplication.mainTabBarController()?.minimizePlayerDetails()
    }
}
