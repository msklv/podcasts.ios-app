//
//  EpisodeCell.swift
//  Podcasts
//
//  Created by Павел Москалев on 5.03.22.
//

import UIKit
import Kingfisher

class EpisodeCell: UITableViewCell {

    @IBOutlet weak var episodeImageView: UIImageView!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func prepareCell(episode: Episode) {
        titleLabel.text = episode.title
        descriptionLabel.text = episode.description
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        dataLabel.text = dateFormatter.string(from: episode.pubDate)
        let url = URL(string: episode.imageUrl?.toSecureHTTPS() ?? "")
        episodeImageView.kf.indicatorType = .activity
        episodeImageView.kf.setImage(with: url)
    }
}
