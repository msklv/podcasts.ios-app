//
//  PlayerDetailsView + Gesture.swift
//  Podcasts
//
//  Created by Павел Москалев on 16.03.22.
//

import Foundation
import UIKit

extension PlayerDetailsView {
    
    @objc func handlePan(gesture: UIPanGestureRecognizer) {
        let coordinat = gesture.translation(in: self.superview)
        let velocity = gesture.velocity(in: self.superview)
        switch gesture.state {
        case .changed: print("changed")
            self.transform = CGAffineTransform(translationX: 0, y:coordinat.y)
            self.miniScreenView.alpha = 1 + coordinat.y / 200
            self.fullScreenStackView.alpha = -coordinat.y / 200
        case .ended:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut) {
                self.transform = .identity
                if coordinat.y < -UIScreen.main.bounds.height / 3 || velocity.y < -500 {
                    UIApplication.mainTabBarController()?.maximizePlayerDetails(episode: nil)
                } else {
                    self.miniScreenView.alpha = 1
                    self.fullScreenStackView.alpha = 0
                }
            }
        @unknown default:
            break
        }
    }
    
    @objc func handleTapMaximize() {
        UIApplication.mainTabBarController()?.maximizePlayerDetails(episode: nil)
    }
}
