//
//  FavoritePodcastCell.swift
//  Podcasts
//
//  Created by Павел Москалев on 22.03.22.
//

import UIKit

class FavoritePodcastCell: UICollectionViewCell {
    
    var podcast : Podcast! {
        didSet {
            nameLabel.text = podcast.trackName
            artistLabel.text = podcast.artistName
            let url  = URL(string: podcast.artworkUrl600 ?? "")
            imageView.kf.setImage(with: url)
        }
    }
    
    let nameLabel = UILabel()
    let artistLabel = UILabel()
    let imageView = UIImageView(image: UIImage(named: "podcastImage"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUI() {
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        artistLabel.font = UIFont.systemFont(ofSize: 14)
        nameLabel.text = "Podcast Name"
        artistLabel.text = "Artist Name"
    }
    
    fileprivate func setupViews() {
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        let stackView = UIStackView(arrangedSubviews: [imageView ,nameLabel,artistLabel])
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
}
